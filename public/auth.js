window.onload = function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');
    var provider = new firebase.auth.GoogleAuthProvider();
    

 

    btnLogin.addEventListener('click', function () {
        var email = txtEmail.value;
        var password = txtPassword.value;

        
        //check if browser supports notification API
        if("Notification" in window)
        {
            if(Notification.permission == "granted")
            {
                var notification = new Notification("NS-SHAFT", {"body":"趕快來打破紀錄啊", "icon":"nails.png"});
            }
            else
            {
                Notification.requestPermission(function (permission) {
                    if (permission === "granted") 
                    {
                        var notification = new Notification("NS-SHAFT", {"body":"趕快來打破紀錄啊", "icon":"nails.png"});
                    }
                });
            }
        }   
        else
        {
            alert("Your browser doesn't support notfication API");
        }       
       
        firebase.auth().signInWithEmailAndPassword(email, password).then(function (result) {
            //var token = result.credential.accessToken;
            //var user = result.user;
            alert("Login Success!");
            document.location.href = "game.html";
        }).catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            alert(errorMessage);
            document.getElementById("inputEmail").value = null;
            document.getElementById("inputPassword").value = null;
            document.location.href = "index.html";
            console.log(error); });

    });

    btnGoogle.addEventListener('click', e => {

        //check if browser supports notification API
        if("Notification" in window)
        {
            if(Notification.permission == "granted")
            {
                var notification = new Notification("NS-SHAFT", {"body":"趕快來打破紀錄啊", "icon":"nails.png"});
            }
            else
            {
                Notification.requestPermission(function (permission) {
                    if (permission === "granted") 
                    {
                        var notification = new Notification("NS-SHAFT", {"body":"趕快來打破紀錄啊", "icon":"nails.png"});
                    }
                });
            }
        }   
        else
        {
            alert("Your browser doesn't support notfication API");
        }       

        console.log('signInWithPopup');
        firebase.auth().signInWithPopup(provider).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
            alert("Login Success!");
            document.location.href = "game.html"
        }).catch(function (error) {
            alert(error.message);
            console.log('error: ' + error.message);
            document.location.href = "index.html"
        });
    });
       

        btnSignUp.addEventListener('click', e => {
            var email = txtEmail.value;
            var password = txtPassword.value;
            firebase.auth().createUserWithEmailAndPassword(email, password).then(function(user) {
                alert("Sign Up Success!");
                document.location.reload = "index.html"
                console.log(user);
                }).catch(function(error) {
                var errorCode = error.code;
                var errorMessage = error.message;
                alert(errorMessage);
                console.log(error); });});

}
// Custom alert

function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}
