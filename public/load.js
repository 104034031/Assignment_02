var loadState = { preload: function () {
    // Add a 'loading...' label on the screen
    var loadingLabel = game.add.text(game.width/2, 150,
    'loading...', { font: '30px Arial', fill: '#ffffff' }); loadingLabel.anchor.setTo(0.5, 0.5);
    // Display the progress bar
    var progressBar = game.add.sprite(game.width/2, 200, 'progressBar'); 
    progressBar.anchor.setTo(0.5, 0.5); 
    game.load.setPreloadSprite(progressBar);
    // Load all game assets
    game.load.image('background', 'background.png');
    game.load.spritesheet('player', 'player.png', 50, 64);
    game.load.image('wall', 'bounder.png');
    game.load.image('ceiling', 'top.png');
    game.load.image('normal', 'normal2.png');
    game.load.image('nails', 'nails.png');
    game.load.spritesheet('shiftRight', 'shift_right.png', 96, 16);
    game.load.spritesheet('shiftLeft', 'shift_left.png', 96, 16);
    game.load.spritesheet('spring', 'spring2.png', 84, 24);
    game.load.spritesheet('broken', 'fake.png', 96, 36); 
    game.load.audio('backsound', 'backsound.wav');
    game.load.audio('fail', 'fail.wav');
    game.load.audio('crash', 'crash.wav');

},
create: function() {
// Go to the menu state 
game.state.start('menu');
} };