var player;
var keyboard;
var platforms = [];
var leftWall;
var rightWall;
var ceiling;
var floor;
var lastTime = 0;
var flag=0;
var life_bar=document.getElementById('life_bar');
var floor_bar=document.getElementById('floor_bar');
var status = 'running';
var backsound;
var failsound;
var crashsound;



var playState={
preload:function () {   //preload()
},

/**************************
 create function
 **************************/

createPlayer:function() {     //player
    player = game.add.sprite(200, 50, 'player');
    player.direction = 10;
    game.physics.arcade.enable(player);
    player.body.gravity.y = 400;
    player.animations.add('left', [3, 4 ,5], 4);
    player.animations.add('right', [6, 7, 8], 4);
    player.animations.add('fall', [0, 1, 2], 8);
    player.life = 100;
    player.unbeatableTime = 0;
    player.touchOn = undefined;  //prevent loop touch
    backsound.play();//play baclground music
},



createBounders:function() {
    leftWall = game.add.sprite(0, 0, 'wall');  //leftwall
    game.physics.arcade.enable(leftWall);
    leftWall.body.immovable = true;

    rightWall = game.add.sprite(508, 0, 'wall');  //rightwall
    game.physics.arcade.enable(rightWall);
    rightWall.body.immovable = true;

    ceiling = game.add.image(0, 0, 'ceiling');
},

createSound:function()
{
    backsound= game.add.audio('backsound');
    failsound= game.add.audio('fail');
    crashsound= game.add.audio('crash');
    backsound.volume=0.5;
    backsound.loop=true;
},

createScoreboard: function()      //scoreboard
{
    var life_text="生命值："+player.life;
    var floor_text="階層："+floor;
    floor=0;
    life_bar.innerHTML=life_text;
    floor_bar.innerHTML=floor_text;
},


showPlatform:function () {    //show platform

    var platform;
    var x = Math.random()*(530 - 96 - 40) + 20;
    var y = 520;
    var num = Math.random() * 100;
    
    if(num < 40) {
        platform = game.add.sprite(x, y, 'normal');
    } else if (num < 50) {
        platform = game.add.sprite(x, y, 'nails');
        game.physics.arcade.enable(platform);
        platform.body.setSize(96, 15, 0, 15);
    } else if (num < 60) {
        platform = game.add.sprite(x, y, 'shiftLeft');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (num < 70) {
        platform = game.add.sprite(x, y, 'shiftRight');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (num < 80) {
        platform = game.add.sprite(x, y, 'spring');
        platform.animations.add('jump', [1,2,1,0], 120);
        platform.frame = 3;
    } else {
        platform = game.add.sprite(x, y, 'broken');
        platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
    }

    game.physics.arcade.enable(platform);
    platform.body.immovable = true;
    platforms.push(platform);
    platform.body.checkCollision.down = false;
    platform.body.checkCollision.left = false;
    platform.body.checkCollision.right = false;
},





movePlayer:function () {
    if(keyboard.left.isDown) {
        player.body.velocity.x = -250;
    } else if(keyboard.right.isDown) {
        player.body.velocity.x = 250;
    } else {
        player.body.velocity.x = 0;
    }
    this.setPlayerAnimate(player);
},

setPlayerAnimate:function(player) {
    var x = player.body.velocity.x;
    var y = player.body.velocity.y;

    if (x < 0 && y > 0) {
        player.animations.play('fall'); 
    }
    if (x > 0 && y > 0) {
        player.animations.play('fall'); 
    }
    if (x < 0 && y == 0) {
        player.animations.play('left');
    }
    if (x > 0 && y == 0) {
        player.animations.play('right');
    }
    if (x == 0 && y != 0) {
        player.animations.play('fall');
    }
    if (x == 0 && y == 0) {
      player.frame = 8;
    }
},
/****************************
update function
*****************************/
createPlatforms:function () {      //platform
    if(game.time.now > lastTime + 600) {
        lastTime = game.time.now;
        if(flag%3==0)
        floor++;
        if(floor>20)
        player.body.velocity.y+=150;
        if(floor>40)
        player.body.velocity.y+=150;
        this.showPlatform();
        flag++;
    }
 
},
updatePlatforms:function () {
    for(var i=0; i<platforms.length; i++) {
        var platform = platforms[i];
        platform.body.position.y -= 2;
        if(platform.body.position.y <= -20) {
            platform.destroy();
            platforms.splice(i, 1);
        }
    }
},

updateScoreboard:function () {
    if(player.life<=0||player.body.y > 500)
    {
    var life_text="你已經死亡";
    var floor_text=floor;
    life_bar.innerHTML=life_text;
    floor_bar.innerHTML=floor_text;
    }
    else
    {
    var life_text=player.life;
    var floor_text=floor;
    life_bar.innerHTML=life_text;
    floor_bar.innerHTML=floor_text;
    }
   
},

effect:function(player, platform) {
    if(platform.key == 'shiftRight') {
        player.body.x += 2;
    }
    if(platform.key == 'shiftLeft') {
        player.body.x -= 2;
    }
    if(platform.key == 'spring') {
        platform.animations.play('jump');
    player.body.velocity.y = -350;
    }
    if(platform.key == 'nails') {
        if (player.touchOn !== platform) {
            player.life -= 10;
            player.touchOn = platform;
            game.camera.flash(0xff0000, 100);
            crashsound.play();
        }
    }
    if(platform.key == 'normal') {
        if (player.touchOn !== platform) {
            if(player.life <30) {
                player.life += 5;
            }
            player.touchOn = platform;
        }
    }
    if(platform.key == 'broken') {
        if(player.touchOn !== platform) {
            platform.animations.play('turn');
            setTimeout(function() {
                platform.body.checkCollision.up = false;
            }, 100);
            player.touchOn = platform;
        }
    }
},

checkTouchCeiling:function(player) {
    if(player.body.y < 0) {
        if(player.body.velocity.y < 0) {
            player.body.velocity.y = 0;
        }
        if(game.time.now > player.unbeatableTime) {
            player.life -= 20;
            game.camera.flash(0xff0000, 100);            //red animation
            crashsound.play();
            player.unbeatableTime = game.time.now + 2000;
        }
    }
},

checkGameOver:function () {
    if(player.life <= 0 || player.body.y > 520) {
        backsound.volume=0;
        failsound.play();
        platforms.forEach(function(s) {s.destroy()});
        platforms = [];
        game.state.start('menu');
    }
},


create:function () {                                              //create()
   
   
    game.add.image(0, 0, 'background'); //background
    keyboard = game.input.keyboard.addKeys({
        'enter': Phaser.Keyboard.ENTER,
        'up': Phaser.Keyboard.UP,
        'down': Phaser.Keyboard.DOWN,
        'left': Phaser.Keyboard.LEFT,
        'right': Phaser.Keyboard.RIGHT,
    });
    this.createSound();
    this.createPlayer();
    this.createBounders();
    this.createScoreboard();
},

update:function () {                                            //update()

    
    if(status == 'gameOver' && keyboard.enter.isDown) this.restart();
    if(status != 'running') return;

    this.physics.arcade.collide(player, platforms, this.effect);
    this.physics.arcade.collide(player, [leftWall, rightWall]);
    this.checkTouchCeiling(player);
    this.checkGameOver();

    this.movePlayer();
    this.updatePlatforms();
    this.updateScoreboard();

    this.createPlatforms();
},
};